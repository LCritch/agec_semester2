#include "Pyramid.h"
#include <fstream>
#include <iostream>
using namespace::std;

Pyramid::Pyramid(){
	Load("Pyramid.txt");
}

Pyramid::~Pyramid(){
	delete indexedVertices;
	delete indices;
	delete normals;
}

bool Pyramid::Load(char* path)
{
	ifstream inFile;
	inFile.open(path);

	if (!inFile.good())
	{
		cerr << "Can't open model file" << path << endl;

		return false;
	}

	inFile >> numVertices;

	if (numVertices > 0)
	{
		indexedVertices = new Vertex3D[numVertices];
		for (int i = 0; i < numVertices; i++)
		{
			inFile >> indexedVertices[i].x;
			inFile >> indexedVertices[i].y;
			inFile >> indexedVertices[i].z;
		}

		/*texCoords = new TexCoord[numVertices];
		for (int i = 0; i < numVertices; i++)
		{
			inFile >> texCoords[i].u;
			inFile >> texCoords[i].v;
		}*/
	}

	inFile >> numNormals;
		if (numNormals > 0)
		{
			normals = new Vector3D[numNormals];
			for (int i = 0; i < numNormals; i++)
			{
				inFile >> normals[i].x;
				inFile >> normals[i].y;
				inFile >> normals[i].z;

			}
		}

	int numTriangles;
	inFile >> numTriangles;
	indexCount = numTriangles * 3;

	if (indexCount > 0)
	{
		indices = new short[indexCount];
		for (int i = 0; i < indexCount; i++)
		{
			inFile >> indices[i];
		}
	}
	inFile.close();
	return true;

}

void Pyramid::Draw()
{
	for (int i = 0; i < indexCount; i += 3)
	{
		DrawTriangle(indices[i], indices[i + 1], indices[i + 2],i);

	}
}

void Pyramid::DrawTriangle(short a, short b, short c, short n)
{
	glBegin(GL_TRIANGLES);
	
	glTexCoord2f(texCoords[a].u, texCoords[a].v);
	glNormal3f(normals[n].x, normals[n].y, normals[n].z);
	glVertex3f(indexedVertices[a].x, indexedVertices[a].y, indexedVertices[a].z);
	glTexCoord2f(texCoords[b].u, texCoords[b].v);
	glNormal3f(normals[n].x, normals[n].y, normals[n].z);
	glVertex3f(indexedVertices[b].x, indexedVertices[b].y, indexedVertices[b].z);
	glTexCoord2f(texCoords[c].u, texCoords[c].v);
	glNormal3f(normals[n].x, normals[n].y, normals[n].z);
	glVertex3f(indexedVertices[c].x, indexedVertices[c].y, indexedVertices[c].z);
	
	glEnd();
}