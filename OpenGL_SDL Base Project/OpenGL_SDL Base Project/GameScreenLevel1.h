#ifndef _GAMESCREENLEVEL1_H
#define _GAMESCREENLEVEL1_H

#include <SDL.h>
#include "GameScreen.h"
#include "Pyramid.h"
#include "object3DS.h"

class GameScreenLevel1 : GameScreen
{
//--------------------------------------------------------------------------------------------------
public:
	GameScreenLevel1();
	~GameScreenLevel1();

	bool		SetUpLevel();
	void		Render();
	void		Update(float deltaTime, SDL_Event e);
	void		SetLight();
	void		SetMaterial();
	void		OutputLine(float x, float y, string text);

//--------------------------------------------------------------------------------------------------
private:
	
	float mCurrentTime;
	Pyramid * pyramid;
	float rotation;
	Object3DS* m_p3DSModel;
	Object3DS* m_Road01;
	Object3DS* m_Road02;
	Object3DS* m_Building;
};


#endif //_GAMESCREENLEVEL1_H