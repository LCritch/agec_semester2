#include "GameScreenLevel1.h"
#include <time.h>
#include <windows.h>
#include <GL\gl.h>
#include <GL\glu.h>
#include <SDL.h>
#include <iostream>
#include <string>
#include <sstream>
#include "../gl/glut.h"
#include "Constants.h"
#include "Commons.h"
#include "Texture.h"
#include "Camera.h"

using namespace::std;

//--------------------------------------------------------------------------------------------------
float xCoordinates;
float yCoordinates;
float zCoordinates;

GameScreenLevel1::GameScreenLevel1() : GameScreen()
{
	srand(time(NULL));
	glEnable(GL_DEPTH_TEST);


	glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	float aspect = (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT;
	gluPerspective(60.0f,aspect,0.1f,1000.0f);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	
	glPushMatrix();
	glPopMatrix();

	glEnable(GL_CULL_FACE);								//Stop calculation of inside faces
	glEnable(GL_DEPTH_TEST);							//Hidden surface removal


	//clear background colour.
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	rotation = 2.0f;

	//Enable Tex2D
	glEnable(GL_TEXTURE_2D);

	Texture2D * texture = new Texture2D();
	texture->Load("Penguins.raw", 512, 512);

	glBindTexture(GL_TEXTURE_2D, texture->GetID());

	//set some parameters so it renders correctly
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	//Set to load the 3DS Model file
	m_p3DSModel = new Object3DS(Vector3D(0.0f, 0.0f, 0.0f), "Octopus.3ds");
	m_Road01 = new Object3DS(Vector3D(0.0f, -5.0f, 0.0f), "HwRoad.3ds");
	m_Road02 = new Object3DS(Vector3D(0.0f, -5.0f, -10.0f), "HwRoad.3ds");
	m_Building = new Object3DS(Vector3D(0.0f, -5.0f, -50.0f), "Build11_3ds.3ds");

	
	//enable lighting
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_NORMALIZE);
}

//--------------------------------------------------------------------------------------------------

GameScreenLevel1::~GameScreenLevel1()
{	
}

//--------------------------------------------------------------------------------------------------



void Draw2DSquare()
{
	glBegin(GL_POLYGON);
	glVertex3f(-0.5f, 0.0f, 0.0f);
	glVertex3f(0.5f, 0.0f, 0.0f);
	glVertex3f(0.5f, -1.0f, 0.0f);
	glVertex3f(-0.5f, -1.0f, 0.0f);
	glEnd();
}

void DrawPoints()
{
	glBegin(GL_POINTS);

	float pointSize = 5.0f;

	//Draw a line of incresing size
	for (float x = 0.0f; x < 4.0f; x += 1.0f)
	{
		for (float y = 0.0f; y < 4.0f; y += 1.0f)
		{
			//Set the point size
			glPointSize(pointSize);

			//draw the point
			glBegin(GL_POINTS);
			glVertex3f(x, y, 0.0f);

			glEnd();

		}
	}

}

	void DrawTriangles()
{
	glBegin(GL_TRIANGLES);

	for (float x = 0.0f; x < 3.0f; x += 1.0f)
	{
		for (float y = 0.0f; y < 3.0f; y += 1.0f)
		{

			//draw the triangle
			glBegin(GL_TRIANGLES);
			glVertex3f(x, y, 0.0f);
			glVertex3f(x-0.5f, y-1.0f, 0.0f);
			glVertex3f(x+0.5f, y-1.0f, 0.0f);
		}
	}
	glEnd();
}

void DrawQuads()
{
	glBegin(GL_QUADS);
	for (float x = 0.0f; x < 3.0f; x += 1.0f)
	{
		for (float y = 0.0f; y < 3.0f; y += 1.0f)
		{

			//draw the triangle
			glBegin(GL_QUADS);
			glVertex3f(x + 0.5f, y - 1.0f, 0.0f);
			glVertex3f(x + 0.5f, y, 0.0f);
			glVertex3f(x - 0.5f, y, 0.0f);
			glVertex3f(x - 0.5f, y - 1.0f, 0.0f);
		}
		
	}
	glEnd();
}


void GameScreenLevel1::SetLight()
{
	lighting light = {
		{ 0.2f, 0.2f, 0.2f, 1.0f },
		{ 0.7f, 0.7f, 0.7f, 1.0f },
		{ 0.5f, 0.5f, 0.5f, 1.0f }
	};

	//position of light in homogeneous coords (x,y,z,w)
	//w should be 0 for directional lights and 1 for spotlights
	float light_pos[] = { 1.0f, 1.0f, 5.0f, 1.0f };

	glLightfv(GL_LIGHT0,GL_AMBIENT,light.ambient);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,light.diffuse);
	glLightfv(GL_LIGHT0,GL_SPECULAR,light.specular);
	glLightfv(GL_LIGHT0,GL_POSITION,light_pos);
}

void GameScreenLevel1::SetMaterial()
{
	material material = {
		{ 0.80f, 0.05f, 0.05f, 1.0f },
		{ 0.80f, 0.05f, 0.05f, 1.0f },
		{ 1.0f, 1.0f, 1.0f, 1.0f },
		100.0f
	};

	glMaterialfv(GL_FRONT,GL_AMBIENT,material.ambient);
	glMaterialfv(GL_FRONT,GL_DIFFUSE,material.diffuse);
	glMaterialfv(GL_FRONT,GL_SPECULAR,material.specular);
	glMaterialfv(GL_FRONT,GL_SHININESS,material.shininess);

}

void DrawTextured2DSquare()
{
	glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-0.5f, -0.5f, 0.0f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(0.5f, -0.5f, 0.0f);
		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(0.5f, 0.5f, 0.0f);
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(-0.5f, 0.5f, 0.0f);
	glEnd();
}


void GameScreenLevel1::OutputLine(float x, float y, string text)
{
	glRasterPos2f(x, y);
	for (int i = 0; i < text.size(); i++)
	{
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, text[i]);
	}

}

void GameScreenLevel1::Update(float deltaTime, SDL_Event e)
{
	Camera::GetInstance()->Update(deltaTime, e);
	mCurrentTime += deltaTime;
	rotation += 0.25f;
	m_p3DSModel->update(deltaTime);

	xCoordinates = Camera::GetInstance()->xCoordinates;
	yCoordinates = Camera::GetInstance()->yCoordinates;
	zCoordinates = Camera::GetInstance()->zCoordinates;


}

//--------------------------------------------------------------------------------------------------

void GameScreenLevel1::Render()
{
	//Clear the screen.
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	

	stringstream X;
	stringstream Y;
	stringstream Z;
	X << "X: " << xCoordinates << endl;
	Y << "Y: " << yCoordinates << endl;
	Z << "Z: " << zCoordinates << endl;

	glColor3f(0.0f, 1.0f, 1.0f);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, 100, 0, 100);
	OutputLine(2, 95, X.str());
	OutputLine(2, 90, Y.str());
	OutputLine(2, 85, Z.str());
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	Camera::GetInstance()->Render();

	m_p3DSModel->render();
	m_Road01->render();
	m_Road02->render();
	m_Building->render();

	SetLight();
	SetMaterial();




	

}



//--------------------------------------------------------------------------------------------------

