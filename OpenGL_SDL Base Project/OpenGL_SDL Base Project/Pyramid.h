#ifndef _PYRAMID_H
#define _PYRAMID_H

#include <Windows.h> //Required for OpenGL on windows
#include <gl/GL.h> //OpenGL Base Header
#include <gl/GLU.h> //OpenGL Utilities
#include "Commons.h"

class Pyramid
{
private:
	Vertex3D * indexedVertices;
	short * indices;

	int numVertices;
	int indexCount;

	int numNormals;
	Vector3D * normals;

	bool Load(char* path);
	void DrawTriangle(short a, short b, short c,short n);

	TexCoord * texCoords;

public:
	Pyramid(void);
	~Pyramid(void);

	void Draw(void);
	void Update(void);


};

#endif