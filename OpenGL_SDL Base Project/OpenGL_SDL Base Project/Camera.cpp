#include "Camera.h"
#include "Constants.h"
#include <math.h>
#include <windows.h>
#include <GL\gl.h>
#include <GL\glu.h>
#include <SDL.h>
#include <iostream>

#include "../gl/glut.h"

using namespace::std;

static Camera* instance = 0;
static float moveSpeed = 2.0f;
static float lookSpeed = 0.5f;


float deltaTime = glutGet(GLUT_ELAPSED_TIME);



Camera::Camera()
{
	moveForward = false;
	moveBackward = false;
	moveLeft = false;
	moveRight = false;

	lookUp = false;
	lookDown = false;
	lookLeft = false;
	lookRight = false;
}

Camera::~Camera()
{

}


Camera* Camera::GetInstance()
{
	if (instance == 0)
	{
		instance = new Camera();
	}
	return instance;
}


void Camera::Update(float deltaTime, SDL_Event e)
{
	

	forward = Vector3D(cos(pitch) * sin(yaw), sin(pitch), cos(pitch) * cos(yaw));

	right = Vector3D(sin(yaw - 3.14f / 2.0f), 0, cos(yaw - 3.14f / 2.0f));

	up = Vector3D((right.y*forward.z) - (right.z*forward.y), (right.z*forward.x) - (right.x*forward.z), (right.x*forward.y) - (right.y*forward.x));

	

	if (e.type == SDL_KEYDOWN)
	{
		switch (e.key.keysym.sym)
		{
		case SDLK_w:
			moveForward= true;
			break;

		case SDLK_s:
			moveBackward = true;
			break;

		case SDLK_d:
			moveRight = true;
			break;

		case SDLK_a:
			moveLeft = true;
			break;

		case SDLK_UP:
			lookUp = true;
			break;

		case SDLK_DOWN:
			lookDown = true;
			break;
			
		case SDLK_LEFT:
			lookLeft = true;
			break;

		case SDLK_RIGHT:
			lookRight = true;
			break;
		}
	}


	if (e.type == SDL_KEYUP)
	{
		switch (e.key.keysym.sym)
		{
		case SDLK_w:
			moveForward = false;
			break;

		case SDLK_s:
			moveBackward = false;
			break;

		case SDLK_d:
			moveRight = false;
			break;

		case SDLK_a:
			moveLeft = false;
			break;

		case SDLK_UP:
			lookUp = false;
			break;

		case SDLK_DOWN:
			lookDown = false;
			break;

		case SDLK_LEFT:
			lookLeft = false;
			break;

		case SDLK_RIGHT:
			lookRight = false;
			break;
		}
	}


	if (moveForward)
	{
		position += forward * moveSpeed*deltaTime;
	}
	if (moveBackward)
	{
		position -= forward * moveSpeed*deltaTime;
	}
	if (moveLeft)
	{
		position -= right * moveSpeed*deltaTime;
	}
	if (moveRight)
	{
		position += right * moveSpeed*deltaTime;
	}

	if (lookUp)
	{
		pitch += lookSpeed * deltaTime;
	}
	if (lookDown)
	{
		pitch -= lookSpeed*deltaTime;
	}
	if (lookLeft)
	{
		yaw += lookSpeed*deltaTime;
	}
	if (lookRight)
	{
		yaw -= lookSpeed*deltaTime;
	}

	xCoordinates = position.x;
	yCoordinates = position.y;
	zCoordinates = position.z;

}



void Camera::Render()
{
	Vector3D lookatPos = position + forward;
	glLoadIdentity();
	gluLookAt(position.x, position.y, position.z, lookatPos.x, lookatPos.y, lookatPos.z, up.x, up.y, up.z);
}

