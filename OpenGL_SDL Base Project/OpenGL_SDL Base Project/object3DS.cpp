#include "object3DS.h"
#include "../gl/glut.h"
#include "3dsLoader.h"

//----------------------------------------------------------------------------------------------------

Object3DS::Object3DS(Vector3D startPosition, string modelFileName)
{
	//start position
	mPosition = startPosition;

	//3ds file to load
	std::strcpy(fileName, modelFileName.c_str());
	loadModel();
	loadTexture();
}

void Object3DS::loadModel()
{
	if (fileName[0] != '---')
		Load3DS(&object, fileName);
}

//----------------------------------------------------------------------------------------------------

void Object3DS::loadTexture()
{
	//TODO: Load a texture to map to the object
	if (textureName[0] != '---')
		object.id_texture = textureName[0];
}

void Object3DS::update(float deltaTime)
{
	//TODO: Move object here.
}

//-----------------------------------------------------------------------------------------------------

void Object3DS::render()
{
	glPushMatrix();
	glTranslatef(mPosition.x, mPosition.y, mPosition.z);

	glBindTexture(GL_TEXTURE_2D, object.id_texture);	//Set the active texture

	glBegin(GL_TRIANGLES);	//glBegin and glEnd delimit the verts that define a primitive
	for (int l_index = 0; l_index < object.triangles_qty; l_index++)
	{
		//------------------------------- FIRST VERTEX ------------------------------------------------
		//Texture cooords of the first vertex
		//glTexCoord2f(object.texcoord[object.triangle[l_index].a].u, object.texcoord[object.triangle[l_index].a].v); //coords of the first vertex
		glVertex3f(object.vertex[object.triangle[l_index].a].x,
			object.vertex[object.triangle[l_index].a].y,
			object.vertex[object.triangle[l_index].a].z);

		//------------------------------- SECOND VERTEX ------------------------------------------------
		//Texture cooords of the second vertex
		//glTexCoord2f(object.texcoord[object.triangle[l_index].b].u, object.texcoord[object.triangle[l_index].b].v); //coords of the second vertex
		glVertex3f(object.vertex[object.triangle[l_index].b].x,
			object.vertex[object.triangle[l_index].b].y,
			object.vertex[object.triangle[l_index].b].z);

		//------------------------------- THIRD VERTEX ------------------------------------------------
		//Texture cooords of the third vertex
		//glTexCoord2f(object.texcoord[object.triangle[l_index].c].u, object.texcoord[object.triangle[l_index].c].v); //coords of the third vertex
		glVertex3f(object.vertex[object.triangle[l_index].c].x,
			object.vertex[object.triangle[l_index].c].y,
			object.vertex[object.triangle[l_index].c].z);
	}

	glEnd();

	glPopMatrix();
}