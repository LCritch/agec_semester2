#ifndef _CAMERA_H
#define _CAMERA_H

#include "Commons.h"
#include "GameScreen.h"
#include <SDL.h>

using namespace::std;

class Camera
{
public:
	Camera();
	~Camera();
	static Camera* GetInstance();
	void Update(float deltaTime, SDL_Event e);
	void Render();

	//booleans for movement functions
	bool moveForward;
	bool moveBackward;
	bool moveLeft;
	bool moveRight;

	//bools for camera rotate functions
	bool lookUp;
	bool lookDown;
	bool lookLeft;
	bool lookRight;

	//floats for displaying XYZ coords on GUI
	float xCoordinates;
	float yCoordinates;
	float zCoordinates;

private:
	Vector3D position = Vector3D(0, 0, 10);
	Vector3D forward = Vector3D();
	Vector3D up = Vector3D();
	Vector3D right = Vector3D();

	float yaw = 3.14f;
	float pitch = 0.0f;
};

#endif